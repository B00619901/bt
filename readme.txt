Node Program	17/02/2016

GENERAL USAGE NOTES
--------------------

- To display items from within the txt file you must list the correct parameters.

- These parameters must run in the following order node(program) index.js(filename)  dependencies.txt(txt file) then the items from the txt file you wish to extract.

- If an item is required that isn't within the txt file an error message will appear

voice:	07749950403	
email:	warwickandrew91@gmail.com

